﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddOccurrencesInArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution s = new Solution();
            Debug.WriteLine(s.solution(new int[] { 0, 1 }));
        }
    }

    class Solution
    {
        public int solution(int[] A)
        {
            if (A.Length % 2 == 0)
                throw new ArgumentException();

            Dictionary<int, bool?> dictionary = new Dictionary<int, bool?>();

            for (int i = 0; i < A.Length; i++)
            {
                if (!dictionary.ContainsKey(A[i]))
                {
                    dictionary.Add(A[i], null);
                }
                else
                {
                    dictionary.Remove(A[i]);
                }
            }

            return dictionary.FirstOrDefault(x => x.Value == null).Key;
        }
    }
}
