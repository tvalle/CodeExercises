﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyclicRotation
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution s = new Solution();

            int[] array = s.solution(new int[] { }, 14);

            foreach (var a in array)
            {
                Debug.Write(a + " ");
            }
        }


    }

    class Solution
    {
        public int[] solution(int[] A, int K)
        {
            List<int> list = new List<int>();

            if (A.Length == 0)
                return new int[] { };

            int positionToBe = K % A.Length;

            list.AddRange(A.ToList<int>().GetRange(A.Length - positionToBe, positionToBe));
            list.Add(A.ToList<int>().ElementAt(0));
            list.AddRange(A.ToList<int>().GetRange(1, A.Length - positionToBe - 1));

            return list.ToArray();
        }
    }
}
